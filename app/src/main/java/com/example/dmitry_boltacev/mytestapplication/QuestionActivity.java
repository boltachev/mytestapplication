package com.example.dmitry_boltacev.mytestapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.dmitry_boltacev.mytestapplication.DataBase.DBHelper;

import java.io.IOException;
import java.util.ArrayList;


public class QuestionActivity extends AppCompatActivity implements View.OnClickListener {


    private Button button1, button2, button3, button4;
    private TextView textView;

    private int idQuestions, idVar1, idVar2, idVar3, idVar4, idTrueVar;

    private Cursor cursor;

    static ArrayList<String> entryVars, dataTrueVars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);

        textView = (TextView) findViewById(R.id.textView);

        entryVars = new ArrayList();
        dataTrueVars = new ArrayList();

        //DataBase
        DBHelper dbHelper = new DBHelper(getApplicationContext());


        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            throw new Error("error");
        }
        try {
            dbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }

        SQLiteDatabase sqLiteDatabase = dbHelper.getMyDataBase();
        cursor = sqLiteDatabase.query(DBHelper.TABLE_NAME, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            setText();
        }

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button1:
                addArrayList(button1.getText().toString());
                Log.e("EntryVar", button1.getTag().toString());
                record();
                break;
            case R.id.button2:
                addArrayList(button2.getText().toString());
                Log.e("EntryVar", button2.getTag().toString());
                record();
                break;
            case R.id.button3:
                addArrayList(button3.getText().toString());
                Log.e("EntryVar", button3.getTag().toString());
                record();
                break;
            case R.id.button4:
                addArrayList(button4.getText().toString());
                Log.e("EntryVar", button4.getTag().toString());
                record();
                break;
        }
    }

    private void addArrayList(String variables) {
        entryVars.add(variables);
    }

    private String getTrueValue(int idTrueVar) {
        return cursor.getString(idTrueVar);//возвращаем строку с правильным ответов по idTrueVar
    }

    private void record() {
        if (cursor.moveToNext()) {
            setText();
        } else {
            Intent intent = new Intent(".ResActivity");
            startActivity(intent);
            finish(); //закрыли QuestionsActivity
        }
    }

    private void setText() {
        idQuestions = cursor.getColumnIndex(DBHelper.KEY_QUESTIONS);
        idVar1 = cursor.getColumnIndex(DBHelper.KEY_VAR1);
        idVar2 = cursor.getColumnIndex(DBHelper.KEY_VAR2);
        idVar3 = cursor.getColumnIndex(DBHelper.KEY_VAR3);
        idVar4 = cursor.getColumnIndex(DBHelper.KEY_VAR4);

        idTrueVar = cursor.getColumnIndex(DBHelper.TRUE_VAR);

        button1.setText(cursor.getString(idVar1));
        button2.setText(cursor.getString(idVar2));
        button3.setText(cursor.getString(idVar3));
        button4.setText(cursor.getString(idVar4));

        dataTrueVars.add(getTrueValue(idTrueVar)); // вернули правильное число

        Log.e("TrueVar", cursor.getString(idTrueVar));

        textView.setText(cursor.getString(idQuestions));
    }

}
