package com.example.dmitry_boltacev.mytestapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static com.example.dmitry_boltacev.mytestapplication.QuestionActivity.dataTrueVars;
import static com.example.dmitry_boltacev.mytestapplication.QuestionActivity.entryVars;

public class ResActivity extends AppCompatActivity {

    private TextView resTextView;

    private int countTrue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res);

        resTextView = (TextView) findViewById(R.id.textViewRes);

        for (String trueVariable: dataTrueVars
             ) {
            for (String entryVariable: entryVars
                 ) {
                if (trueVariable.equals(entryVariable)) {
                    countTrue++;
                }
            }
        }

        resTextView.setText(String.valueOf(countTrue) + " of " + dataTrueVars.size());


    }

}
