package com.example.dmitry_boltacev.mytestapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dmitry_boltacev.mytestapplication.R;

/**
 * Created by dmitry_boltacev on 08.02.17.
 */

public class VariablesFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.variables_layout, container, false);
    }
}
