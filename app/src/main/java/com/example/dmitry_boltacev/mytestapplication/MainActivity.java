package com.example.dmitry_boltacev.mytestapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    /*
    * Проект был одной моей из самых первых попытов в андройд, но тут я более менее чисто писал, как мне кажется.
    * После него я захотел сделать все более менее качественно, т.е не считывание данных из БД, а случайная генерация
    * этот проект я скинул Игорю с сылкой на архив, там я использовал уже SharedPreferences для задания диапазона
    *
    * вот тут в архиве более-менее версия, как мне кажется, если ее до ума довести, то даже можно выложить
    * https://drive.google.com/file/d/0B6xVdYxDf0tUeDBmMmhGVnB6M2c/viewпше
    * */

    Intent startTest;//Старт теста

    Button startBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //из начальной активити в заданные
        startTest = new Intent(MainActivity.this, QuestionActivity.class);

        //поиск кнопок по id
        startBtn = (Button) findViewById(R.id.start_btn);

        startBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        //переход на новые активити по нажатию кнопки

        switch (view.getId()) {
            case R.id.start_btn:
                startActivity(startTest);
                break;
        }

    }
}
